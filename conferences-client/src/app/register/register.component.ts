import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserTo } from '../datamodel/user.to';
import { AppConstants } from '../app.constants';
import { LectureTo } from '../datamodel/lecture.to';
import { UserWithLectureTo } from '../datamodel/user-with-lecture.to';

@Component({
  selector: 'conf-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  constructor(private http: HttpClient) { }

  
  @Output()
  registered = new EventEmitter<void>();

  onRegisterClick() {
    let login = document.getElementById("inputNewLogin") as any;
    let password = document.getElementById("inputNewPassword") as any;
    let firstName = document.getElementById("inputNewFirstName") as any;
    let lastName = document.getElementById("inputNewLastName") as any;
    let email = document.getElementById("inputNewEmail") as any;
    let asLecturer = document.getElementById('inputAsLecturer') as any;
    let user = new UserTo();
    user.login = login.value;
    user.password = password.value;
    user.firstName = firstName.value;
    user.lastName = lastName.value;
    user.email = email.value;
    user.admin = false;
    user.lecturer = asLecturer.checked;
    user.price = 20;
    user.alreadyPaid = false;
    user.lectureRegistered = false;
    user.verified = false;
    user.rodoAccepted = false;
    let lecture = new LectureTo;
    if (user.lecturer) {
      let title = document.getElementById('inputNewLectureTitle') as any;
      let abstract = document.getElementById('inputNewAbstract') as any;
      lecture.accepted = false;
      lecture.lectureAbstract = abstract.value;
      lecture.topic = title.value;
    }
    this.http.post(AppConstants.SERVER_ADDRESS + "register", user).subscribe(data => {
      if(user.lecturer) {
        let userWithLecture = new UserWithLectureTo();
        userWithLecture.user = user;
        userWithLecture.lecture = lecture;
        this.http.post(AppConstants.SERVER_ADDRESS + "registerLecture", userWithLecture).subscribe();
      }
      alert("New user registered.");
      this.registered.emit();
    });
  }

  registeringAsLecturer() {
    let asLecturer = document.getElementById('inputAsLecturer') as any;
    return asLecturer.checked;
  }
}
