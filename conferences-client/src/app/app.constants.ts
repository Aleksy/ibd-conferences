import connection from 'connection.config.json'

export class AppConstants {
    public static readonly SERVER_ADDRESS = "http://" + connection.serverHost + ":"
                                            + connection.serverPort + "/"
                                            + connection.serverAppName + "/";
}
