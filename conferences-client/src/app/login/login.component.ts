import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from '../app.constants';
import { UserTo } from '../datamodel/user.to';
import { AppComponent } from '../app.component';
import { UserWithLectureTo } from '../datamodel/user-with-lecture.to';

@Component({
  selector: 'conf-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  @Output()
  logged = new EventEmitter<UserWithLectureTo>();

  constructor(private http: HttpClient) { }

  onLoginClick() {
    let login = document.getElementById("inputLogin") as any;
    let password = document.getElementById("inputPassword") as any;
    let user = new UserTo();
    user.login = login.value;
    user.password = password.value;
    this.http.post(AppConstants.SERVER_ADDRESS + "login", user).subscribe(data => {
      if (data === null) {
        alert("Wrong login or password.");
      } else {
        this.logged.emit(data as UserWithLectureTo);
      }
    });
  }
}
