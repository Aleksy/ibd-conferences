import { Component, EventEmitter, Output } from '@angular/core';
import { UserTo } from './datamodel/user.to';
import { ConferenceTo } from './datamodel/conference.to';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from './app.constants';
import { Subject } from 'rxjs';
import { LectureTo } from './datamodel/lecture.to';
import { UserWithLectureTo } from './datamodel/user-with-lecture.to';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private LOGIN_COMPONENT = true;
  private REGISTER_COMPONENT = false;
  private MAIN_MENU_COMPONENT = false;
  private EDIT_CONFERENCE_COMPONENT = false;
  private USER_LIST_COMPONENT = false;
  private UPDATE_USER_COMPONENT = false;
  private loggedUser: UserWithLectureTo;
  private conference: ConferenceTo;
  private lectures: LectureTo[];
  private users: UserWithLectureTo[];

  private conferenceLoaded = new Subject<ConferenceTo>();

  constructor(private http: HttpClient) { }

  onRegisterNewUserButtonClick() {
    this.falseWholeStateExcept(() => this.REGISTER_COMPONENT = true);
  }

  onLoginPageButtonClick() {
    this.falseWholeStateExcept(() => this.LOGIN_COMPONENT = true);
  }

  onLogged($event: UserWithLectureTo) {
    this.loggedUser = $event;

    this.http.get(AppConstants.SERVER_ADDRESS + "getAllConferences").subscribe((data) => {
      if (data !== null) {
        this.conference = data[0].conference;
        this.lectures = data[0].lectures;
      } else {
        this.conference = null;
        this.lectures = null;
      }

      this.conferenceLoaded.next(this.conference);
      if (!this.conference && this.loggedUser.user.admin) {
        this.falseWholeStateExcept(() => this.EDIT_CONFERENCE_COMPONENT = true);
      } else {
        this.http.get(AppConstants.SERVER_ADDRESS + "getUsers").subscribe(data => {
          this.users = data as UserWithLectureTo[];
          this.falseWholeStateExcept(() => this.MAIN_MENU_COMPONENT = true);
        });
        
      }
    });
  }

  onRemoveConference() {
    let remove: boolean = confirm("Would you really like to delete all conference data? Every user and lecture also will be deleted. Cannot be undone.");
    if (remove === true) {
      this.http.post(AppConstants.SERVER_ADDRESS + "clearConferenceData", this.conference).subscribe(() => {
        this.conference = null;
        this.falseWholeStateExcept(() => this.EDIT_CONFERENCE_COMPONENT = true);
      });
    }
  }

  lectureLocalizationChanged(lecture, $event) {
    let savedLecture = this.lectures.filter(l => l.id === lecture.id)[0];
    let index = this.lectures.indexOf(savedLecture);
    savedLecture.localization = $event.target.value
    this.lectures[index] = savedLecture;
  }

  lectureDateAndTimeChanged(lecture, $event) {
    let savedLecture = this.lectures.filter(l => l.id === lecture.id)[0];
    let index = this.lectures.indexOf(savedLecture);
    savedLecture.timeAndDate = $event.target.value;

    this.lectures[index] = savedLecture;
  }

  onRegistered() {
    this.falseWholeStateExcept(() => this.LOGIN_COMPONENT = true);
  }

  saveLectureChanges() {
    this.http.post(AppConstants.SERVER_ADDRESS + "lecturesUpdate", this.lectures).subscribe(data => {
      this.falseWholeStateExcept(() => this.MAIN_MENU_COMPONENT = true);
    });
  }

  onUpdateConference() {
    this.falseWholeStateExcept(() => this.EDIT_CONFERENCE_COMPONENT = true)
    this.conferenceLoaded.next(this.conference);
  }

  private falseWholeStateExcept(trueComponent: Function) {
    this.LOGIN_COMPONENT = false;
    this.REGISTER_COMPONENT = false;
    this.MAIN_MENU_COMPONENT = false;
    this.EDIT_CONFERENCE_COMPONENT = false;
    this.USER_LIST_COMPONENT = false;
    this.UPDATE_USER_COMPONENT = false;
    trueComponent();
  }

  createConference() {
    this.conference = new ConferenceTo();
    this.fillValues();
    this.http.post(AppConstants.SERVER_ADDRESS + "registerConference", this.conference).subscribe(data => {
      this.falseWholeStateExcept(() => this.MAIN_MENU_COMPONENT = true);
    });
  }

  updateConference() {
    this.conference = new ConferenceTo();
    this.fillValues();
    this.http.post(AppConstants.SERVER_ADDRESS + "updateConference", this.conference).subscribe(data => {
      this.falseWholeStateExcept(() => this.MAIN_MENU_COMPONENT = true);
    });
  }

  onSignOut() {
    this.loggedUser = null;
    this.falseWholeStateExcept(() => this.LOGIN_COMPONENT = true);
  }

  onUpdateData() {
    this.falseWholeStateExcept(() => this.UPDATE_USER_COMPONENT = true);
  }

  newPasswordTyped() {
    let psw = document.getElementById("inputUpdatePassword") as any;
    return psw.value.length > 0;
  }

  shouldShowLecture(lecture) {
    return this.loggedUser.user.admin || lecture.accepted;
  }

  deleteLecture(lecture) {
    let remove: boolean = confirm("Would you really like to delete lecture " + lecture.topic + "? Cannot be undone.");
    if (remove === true) {
      this.http.post(AppConstants.SERVER_ADDRESS + "lectureDelete", lecture).subscribe(() => {
        this.onLogged(this.loggedUser);
      });
    }
  }

  shouldShowButtons(lecture) {
    return this.loggedUser.user.admin && !lecture.accepted;
  }

  deleteUser(user: UserWithLectureTo) {
    let remove: boolean = confirm("Would you really like to delete user " + user.user.login + "? Cannot be undone.");
    if (remove === true) {
      this.http.post(AppConstants.SERVER_ADDRESS + "userDelete", user).subscribe(() => {
        this.users = this.users.filter(u => u.user.login !== user.user.login);
        this.falseWholeStateExcept(() => this.USER_LIST_COMPONENT = true);
      });
    }
  }


  acceptLecture(lecture) {
    lecture.accepted = true;
    this.http.post(AppConstants.SERVER_ADDRESS + "lectureUpdate", lecture).subscribe(() =>
      this.http.get(AppConstants.SERVER_ADDRESS + "getLectures").subscribe((data) => this.lectures = data as LectureTo[]));
  }

  onUserList() {
    this.http.get(AppConstants.SERVER_ADDRESS + "getUsers").subscribe((data) => {
      this.users = data as UserWithLectureTo[];
      this.falseWholeStateExcept(() => this.USER_LIST_COMPONENT = true);
    });
  }

  setPaid(user) {
    user.user.alreadyPaid = true;
    this.http.post(AppConstants.SERVER_ADDRESS + "userUpdate", user).subscribe((data) => {
      this.falseWholeStateExcept(() => this.USER_LIST_COMPONENT = true);
    });
  }

  goToMainMenu() {
    this.falseWholeStateExcept(() => this.MAIN_MENU_COMPONENT = true);
  }

  determineLecturer(lecture) {
    let lecturer = this.users.filter(user => user && user.lecture && user.lecture.id === lecture.id)[0]
    return lecturer.user.firstName + " " + lecturer.user.lastName;  
  }

  acceptRodo() {
    this.loggedUser.user.rodoAccepted = true;
    this.http.post(AppConstants.SERVER_ADDRESS + "userUpdate", this.loggedUser).subscribe();
  }

  onUpdateUserClick() {
    let firstName = document.getElementById("inputUpdateFirstName") as any;
    let lastName = document.getElementById("inputUpdateLastName") as any;
    let email = document.getElementById("inputUpdateEmail") as any;
    this.loggedUser.user.firstName = firstName.value;
    this.loggedUser.user.lastName = lastName.value;
    this.loggedUser.user.email = email.value;
    if (this.loggedUser.user.lecturer && !this.loggedUser.lecture.accepted) {
      let topic = document.getElementById("inputUpdateLectureTitle") as any;
      let abstract = document.getElementById("inputUpdateAbstract") as any;
      this.loggedUser.lecture.lectureAbstract = abstract.value;
      this.loggedUser.lecture.topic = topic.value;
    }
    this.http.post(AppConstants.SERVER_ADDRESS + "userUpdate", this.loggedUser).subscribe((data) => {
      this.goToMainMenu();
    });
  }

  private fillValues() {
    let title = document.getElementById("inputConferenceTitle") as any;
    let host = document.getElementById("inputConferenceHost") as any;
    let dateAndTime = document.getElementById("inputConferenceDateAndTime") as any;
    let localization = document.getElementById("inputConferenceLocalization") as any;
    this.conference.title = title.value;
    this.conference.host = host.value;
    this.conference.dateAndTime = dateAndTime.value;
    this.conference.localization = localization.value;
  }
}
