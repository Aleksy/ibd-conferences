import { UserTo } from './user.to';
import { LectureTo } from './lecture.to';

export class UserWithLectureTo {
    user: UserTo;
    lecture: LectureTo;
}