import { LectureTo } from './lecture.to';

export class UserTo {
    id: number;
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    admin: boolean;
    lecturer: boolean;
    price: number;
    alreadyPaid: boolean;
    lectureRegistered: boolean;
    verified: boolean;
    rodoAccepted: boolean;
}