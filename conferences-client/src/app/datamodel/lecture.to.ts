export class LectureTo {
    id: number;
    topic: string;
    lectureAbstract: string;
    localization: string;
    timeAndDate: Date;
    accepted: boolean;
}