import { ConferenceTo } from './conference.to';
import { LectureTo } from './lecture.to';

export class ConferenceWithLecturesTo {
    conference: ConferenceTo;
    lectures: LectureTo[];
}