export class ConferenceTo {
    id: number;
    title: string;
    host: string;
    dateAndTime: string;
    localization: string;
}