package destr.pwr.dpp.conferences.common.model;

import java.util.List;

public class ConferenceWithLecturesTo {
   private Conference conference;
   private List<Lecture> lectures;

   /**
    * Getter for conference
    * @return conference {@link Conference}
    */
   public Conference getConference() {
      return conference;
   }

   /**
    * Setter for conference
    *
    * @param conference conference to set
    */
   public void setConference(Conference conference) {
      this.conference = conference;
   }

   /**
    * Getter for lectures
    * @return lectures
    */
   public List<Lecture> getLectures() {
      return lectures;
   }

   /**
    * Setter for lectures
    *
    * @param lectures lectures to set
    */
   public void setLectures(List<Lecture> lectures) {
      this.lectures = lectures;
   }
}
