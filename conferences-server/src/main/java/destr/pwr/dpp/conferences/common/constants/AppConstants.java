package destr.pwr.dpp.conferences.common.constants;

/**
 * Descriptive Constants container
 */
public class AppConstants {
   /**
    * Name of application
    */
   public static final String NAME = "Conferences Server";
   /**
    * Version of application (needs to be upgraded with POM project version)
    */
   public static final String VERSION = "0.3.0";
   /**
    * POM artifact id used to setting connection with database
    */
   public static final String ARTIFACT_ID = "conferences";
   /**
    * Authors of application
    */
   public static final String AUTHORS = "Aleksy Bernat, Patryk Ekiert, Michal Kaczmarek";
}
