package destr.pwr.dpp.conferences.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "CONFERENCE")
public class Conference implements Serializable {
    static final long serialVersionUID = 1L;

    @Id
    private Integer id;

    @NotNull
    @Column(name = "TITLE")
    private String title;
    @NotNull
    @Column(name = "HOST")
    private String host;
    @Column(name = "DATE_AND_TIME")
    private String dateAndTime;
    @Column(name = "LOCALIZATION")
    private String Localization;

    /**
     * Getter for id
     * @return id {@link Integer}
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for id
     *
     * @param id id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for title
     * @return title {@link String}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter for title
     *
     * @param title title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for host
     * @return host {@link String}
     */
    public String getHost() {
        return host;
    }

    /**
     * Setter for host
     *
     * @param host host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Getter for dateAndTime
     * @return dateAndTime {@link String}
     */
    public String getDateAndTime() {
        return dateAndTime;
    }

    /**
     * Setter for dateAndTime
     *
     * @param dateAndTime dateAndTime to set
     */
    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    /**
     * Getter for localization
     * @return localization {@link String}
     */
    public String getLocalization() {
        return Localization;
    }

    /**
     * Setter for localization
     *
     * @param localization localization to set
     */
    public void setLocalization(String localization) {
        Localization = localization;
    }
}
