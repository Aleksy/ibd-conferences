package destr.pwr.dpp.conferences.common.rest.api;

import destr.pwr.dpp.conferences.common.model.*;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

/**
 * Conferences Rest Service class
 */
@RequestMapping("/conferences")
@CrossOrigin(origins = "http://localhost:4200")
public interface ConferencesRestService {

   /**
    * Gets price for the given user. Lecturers got different price.
    * @param user User to receive price
    * @return Price for the given user
    */
   @PostMapping("/getPrice")
   int getPriceForUser(@RequestBody User user);

   /**
    * Tries to log in the given user. Will return null if operation fails.
    * @param user User to be logged in
    * @return User with lecture
    * @throws SQLException Database exception
    */
   @PostMapping("/login")
   UserWithLectureTo login(@RequestBody User user) throws SQLException;

   /**
    * Registers given user
    * @param user User to register
    * @throws SQLException Database exception
    */
   @PostMapping("/register")
   void registerUser(@RequestBody User user) throws SQLException;

   /**
    * Updates the data for the User
    * @param user User to update
    */
   @PostMapping("/userUpdate")
   void updateUser(@RequestBody UserWithLectureTo user);

   /**
    * Deletes given user
    * @param user User to delete
    */
   @PostMapping("/userDelete")
   void deleteUser(@RequestBody UserWithLectureTo user);

   /**
    * Gets all users with associated lectures
    * @return List of the users with lecture (if exist)
    * @throws SQLException Database exception
    */
   @GetMapping("/getUsers")
   List<UserWithLectureTo> getAllUsers() throws SQLException;

   /**
    * Gets all lectures
    * @return List of all lectures
    * @throws SQLException Database exception
    */
   @GetMapping("/getLectures")
   List<Lecture> getAllLectures() throws SQLException;

   /**
    * Gets all lectures of given status of acceptance
    * @param isAccepted Is lecture accepted by the admin?
    * @return List of lectures accepted by admin or not
    * @throws SQLException Database exception
    */
   @GetMapping("/getLecturesOfStatus")
   List<Lecture> getAllLecturesOfStatus(@RequestBody boolean isAccepted) throws SQLException;

   /**
    * Registers lecture for given user
    * @param userWithLectureTo User that lecture will be added to.
    * @throws SQLException Database exception
    */
   @PostMapping("/registerLecture")
   void registerLecture(@RequestBody UserWithLectureTo userWithLectureTo) throws SQLException;

   /**
    * Updates Lecture data, either for user part or admin part.
    * Topic and abstract is filled by user.
    * Localization, Date and order number is handled by Admin.
    * @param lecture Lecture to update
    * @throws SQLException Database exception
    */
   @PostMapping("/lectureUpdate")
   void updateLecture(@RequestBody Lecture lecture) throws SQLException;

   /**
    * Updates Lectures data, either for user part or admin part.
    * Topic and abstract is filled by user.
    * Localization, Date and order number is handled by Admin.
    * @param lecture Lecture to update
    * @throws SQLException Database exception
    */
   @PostMapping("/lecturesUpdate")
   void updateLectures(@RequestBody List<Lecture> lecture) throws SQLException;

   /**
    * Deletes given lecture from database.
    * @param lecture Lecture to delete
    * @throws SQLException Database exception
    */
   @PostMapping("/lectureDelete")
   void deleteLecture(@RequestBody Lecture lecture) throws SQLException;

   /**
    * Gets all users with certain payment status (either paid or not paid)
    * @param isAlreadyPaid Should get the users that already paid or not
    * @return List of users with given status of payment
    * @throws SQLException Database exception
    */
   @PostMapping("/getAllUsersOfPaymentStatus")
   List<User> getAllUsersOfPaymentStatus(@RequestBody boolean isAlreadyPaid) throws SQLException;

   /**
    * Gets all conferences
    * @return List of all conferences
    * @throws SQLException Database exception
    */
   @GetMapping("/getAllConferences")
   List<ConferenceWithLecturesTo> getAllConferences() throws SQLException;

   /**
    * Clears conference data, deletes all lectures and users except admin
    * @param conference conference to clear
    * @throws SQLException Database exception
    */
   @PostMapping("/clearConferenceData")
   void clearConferenceData(@RequestBody Conference conference) throws SQLException;

   /**
    * Updates given conference
    * @param conference Conference to update
    * @throws SQLException Database exception
    */
   @PostMapping("/updateConference")
   void updateConference(@RequestBody Conference conference) throws SQLException;

   /**
    * Registers conference
    * @param conference conference to register
    * @throws SQLException Database exception
    */
   @PostMapping("/registerConference")
   void registerConference(@RequestBody Conference conference) throws SQLException;


}
