package destr.pwr.dpp.conferences.common.exception;

/**
 * Abstract business exception
 */
public abstract class ConferencesException extends Exception {
   /**
    * The constructor
    * @param message to print
    */
   public ConferencesException(String message) {
      super(message);
   }
}
