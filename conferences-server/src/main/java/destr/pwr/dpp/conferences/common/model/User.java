package destr.pwr.dpp.conferences.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * User model class
 */
@Entity
@Table(name = "USER")
public class User implements Serializable {
   static final long serialVersionUID = 1L;
   @Id
   private Integer id;
   @NotNull
   @Column(name = "LOGIN")
   private String login;
   @NotNull
   @Column(name = "PASSWORD")
   private String password;
   @NotNull
   @Column(name = "FIRST_NAME")
   private String firstName;
   @NotNull
   @Column(name = "LAST_NAME")
   private String lastName;
   @NotNull
   @Column(name = "EMAIL")
   private String email;
   @NotNull
   @Column(name = "ADMIN")
   private Boolean admin;
   @NotNull
   @Column(name = "LECTURER")
   private Boolean lecturer;
   @NotNull
   @Column(name = "PRICE")
   private Integer price;
   @NotNull
   @Column(name = "ALREADY_PAID")
   private Boolean alreadyPaid;
   @NotNull
   @Column(name = "LECTURE_REGISTERED")
   private Boolean lectureRegistered;
   @NotNull
   @Column(name = "RODO_ACCEPTED")
   private Boolean rodoAccepted;
   @NotNull
   @Column(name = "VERIFIED")
   private Boolean verified;
   @Column(name = "LECTURE_ID")
   private Integer lectureId;

   /**
    * Getter for id
    * @return id {@link Integer}
    */
   public Integer getId() {
      return id;
   }

   /**
    * Setter for id
    *
    * @param id id to set
    */
   public void setId(Integer id) {
      this.id = id;
   }

   /**
    * Getter for login
    * @return login {@link String}
    */
   public String getLogin() {
      return login;
   }

   /**
    * Setter for login
    *
    * @param login login to set
    */
   public void setLogin(String login) {
      this.login = login;
   }

   /**
    * Getter for password
    * @return password {@link String}
    */
   public String getPassword() {
      return password;
   }

   /**
    * Setter for password
    *
    * @param password password to set
    */
   public void setPassword(String password) {
      this.password = password;
   }

   /**
    * Getter for firstName
    * @return firstName {@link String}
    */
   public String getFirstName() {
      return firstName;
   }

   /**
    * Setter for firstName
    *
    * @param firstName firstName to set
    */
   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   /**
    * Getter for lastName
    * @return lastName {@link String}
    */
   public String getLastName() {
      return lastName;
   }

   /**
    * Setter for lastName
    *
    * @param lastName lastName to set
    */
   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   /**
    * Getter for email
    * @return email {@link String}
    */
   public String getEmail() {
      return email;
   }

   /**
    * Setter for email
    *
    * @param email email to set
    */
   public void setEmail(String email) {
      this.email = email;
   }

   /**
    * Getter for admin
    * @return admin {@link Boolean}
    */
   public Boolean isAdmin() {
      return admin;
   }

   /**
    * Setter for admin
    *
    * @param admin admin to set
    */
   public void setAdmin(Boolean admin) {
      this.admin = admin;
   }

   /**
    * Getter for lecturer
    * @return lecturer {@link Boolean}
    */
   public Boolean isLecturer() {
      return lecturer;
   }

   /**
    * Setter for lecturer
    *
    * @param lecturer lecturer to set
    */
   public void setLecturer(Boolean lecturer) {
      this.lecturer = lecturer;
   }

   /**
    * Getter for price
    * @return price {@link Integer}
    */
   public Integer getPrice() {
      return price;
   }

   /**
    * Setter for price
    *
    * @param price price to set
    */
   public void setPrice(Integer price) {
      this.price = price;
   }

   /**
    * Getter for alreadyPaid
    * @return alreadyPaid {@link Boolean}
    */
   public Boolean isAlreadyPaid() {
      return alreadyPaid;
   }

   /**
    * Setter for alreadyPaid
    *
    * @param alreadyPaid alreadyPaid to set
    */
   public void setAlreadyPaid(Boolean alreadyPaid) {
      this.alreadyPaid = alreadyPaid;
   }

   /**
    * Getter for lectureRegistered
    * @return lectureRegistered {@link Boolean}
    */
   public Boolean isLectureRegistered() {
      return lectureRegistered;
   }

   /**
    * Setter for lectureRegistered
    *
    * @param lectureRegistered lectureRegistered to set
    */
   public void setLectureRegistered(Boolean lectureRegistered) {
      this.lectureRegistered = lectureRegistered;
   }

   /**
    * Getter for verified
    * @return verified {@link Boolean}
    */
   public Boolean isVerified() {
      return verified;
   }

   /**
    * Setter for verified
    *
    * @param verified verified to set
    */
   public void setVerified(Boolean verified) {
      this.verified = verified;
   }

   /**
    * Getter for lectureId
    * @return lectureId {@link Integer}
    */
   public Integer getLectureId() {
      return lectureId;
   }

   /**
    * Setter for lectureId
    *
    * @param lectureId lectureId to set
    */
   public void setLectureId(Integer lectureId) {
      this.lectureId = lectureId;
   }

   /**
    * Getter for rodoAccepted
    * @return rodoAccepted {@link Boolean}
    */
   public Boolean isRodoAccepted() {
      return rodoAccepted;
   }

   /**
    * Setter for rodoAccepted
    *
    * @param rodoAccepted rodoAccepted to set
    */
   public void setRodoAccepted(Boolean rodoAccepted) {
      this.rodoAccepted = rodoAccepted;
   }
}
