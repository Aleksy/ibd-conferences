package destr.pwr.dpp.conferences.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

/**
 * Lecture model class
 */
@Entity
@Table(name = "LECTURE")
public class Lecture implements Serializable {
   static final long serialVersionUID = 1L;

   @Id
   private Integer id;
   @NotNull
   @Column(name = "TOPIC")
   private String topic;

   @NotNull
   @Column(name = "ABSTRACT")
   private String lectureAbstract;

   @Column(name = "LOCALIZATION")
   private String localization;

   @Column(name = "TIME_AND_DATE")
   private Date timeAndDate;

   @Column(name = "ACCEPTED")
   private Boolean accepted;

   /**
    * Getter for id
    * @return id {@link Integer}
    */
   public Integer getId() {
      return id;
   }

   /**
    * Setter for id
    *
    * @param id id to set
    */
   public void setId(Integer id) {
      this.id = id;
   }

   /**
    * Getter for topic
    * @return topic {@link String}
    */
   public String getTopic() {
      return topic;
   }

   /**
    * Setter for topic
    *
    * @param topic topic to set
    */
   public void setTopic(String topic) {
      this.topic = topic;
   }

   /**
    * Getter for lectureAbstract
    * @return lectureAbstract {@link String}
    */
   public String getLectureAbstract() {
      return lectureAbstract;
   }

   /**
    * Setter for lectureAbstract
    *
    * @param lectureAbstract lectureAbstract to set
    */
   public void setLectureAbstract(String lectureAbstract) {
      this.lectureAbstract = lectureAbstract;
   }

   /**
    * Getter for localization
    * @return localization {@link String}
    */
   public String getLocalization() {
      return localization;
   }

   /**
    * Setter for localization
    *
    * @param localization localization to set
    */
   public void setLocalization(String localization) {
      this.localization = localization;
   }

   /**
    * Getter for timeAndDate
    * @return timeAndDate {@link String}
    */
   public Date getTimeAndDate() {
      return timeAndDate;
   }

   /**
    * Setter for timeAndDate
    *
    * @param timeAndDate timeAndDate to set
    */
   public void setTimeAndDate(Date timeAndDate) {
      this.timeAndDate = timeAndDate;
   }

   /**
    * Getter for accepted
    * @return accepted {@link Boolean}
    */
   public Boolean isAccepted() {
      return accepted;
   }

   /**
    * Setter for accepted
    *
    * @param accepted accepted to set
    */
   public void setAccepted(Boolean accepted) {
      this.accepted = accepted;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Lecture lecture = (Lecture) o;
      return Objects.equals(topic, lecture.topic) &&
         Objects.equals(lectureAbstract, lecture.lectureAbstract) &&
         Objects.equals(localization, lecture.localization) &&
         Objects.equals(timeAndDate, lecture.timeAndDate) &&
         Objects.equals(accepted, lecture.accepted);
   }

   @Override
   public int hashCode() {
      return Objects.hash(topic, lectureAbstract, localization, timeAndDate, accepted);
   }
}
