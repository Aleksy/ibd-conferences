package destr.pwr.dpp.conferences.common.model;

public class UserWithLectureTo {
   private User user;
   private Lecture lecture;

   /**
    * Getter for user
    * @return user {@link User}
    */
   public User getUser() {
      return user;
   }

   /**
    * Setter for user
    *
    * @param user user to set
    */
   public void setUser(User user) {
      this.user = user;
   }

   /**
    * Getter for lecture
    * @return lecture {@link Lecture}
    */
   public Lecture getLecture() {
      return lecture;
   }

   /**
    * Setter for lecture
    *
    * @param lecture lecture to set
    */
   public void setLecture(Lecture lecture) {
      this.lecture = lecture;
   }
}
