package destr.pwr.dpp.conferences.dataaccess.api;

import destr.pwr.dpp.conferences.common.model.Conference;
import destr.pwr.dpp.conferences.common.model.Lecture;
import destr.pwr.dpp.conferences.common.model.User;
import java.sql.SQLException;
import java.util.List;

/**
 * Conference data access object
 */
public interface ConferenceDao {

   /**
    *  Returns list of all conferences
    *
    * @return List of all conferences
    */
   List<Conference> getAllConferences();

   /**
    * Creates conference instance in database
    * @param conference Conference to create
    */
   void reigsterConference(Conference conference);

   /**
    * Updates given conference in database
    * @param conference Conference to update
    */
   void updateConference(Conference conference);

   /**
    * Deletes given conference from database
    * @param conference Conference reference to delete
    */
   void deleteConference(Conference conference);

}

