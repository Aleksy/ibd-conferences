package destr.pwr.dpp.conferences;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Conferences Application main class
 */

@SpringBootApplication
@EnableAutoConfiguration
public class ConferencesApp {

   /**
    * Main method
    *
    * @param args list
    */
   public static void main(String[] args) {
      SpringApplication.run(ConferencesApp.class, args);
   }
}
