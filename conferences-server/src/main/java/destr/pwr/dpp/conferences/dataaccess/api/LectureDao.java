package destr.pwr.dpp.conferences.dataaccess.api;

import destr.pwr.dpp.conferences.common.model.Lecture;
import destr.pwr.dpp.conferences.common.model.User;
import java.sql.SQLException;
import java.util.List;

/**
 * Lecture data access object
 */
public interface LectureDao {

    /**
     * get lecture by lecture id
     * user can have only one llecture
     *
     * @param lectureId id of the lecture
     * @return Found lecture of given id
     * @throws SQLException Database exception
     */
    Lecture getLecture(Integer lectureId) throws SQLException;

    /**
     * get all lectures from database
     *
     * @return List of all lectures
     * @throws SQLException Database exception
     */
    List<Lecture> getAllLectures() throws SQLException;

    /**
     * get all lectures of status isAccepted or not
     * @param  isAccepted Get list of accepted or not accepted lectures.
     * @return List of lectures of certain status of acceptance
     * @throws SQLException Database exception
     */
    List<Lecture> getAllLecturesOfStatus(boolean isAccepted) throws SQLException;

    /**
     * save lecture in database
     *
     * @param lecture Lecture to add
     * @return newly created lecture, if failed - null
     * @throws SQLException Database exception
     */
    Lecture newLecture(Lecture lecture) throws SQLException;


   /**
    *  updates given lecture
    *
    * @param lecture Lecture to update
    */
   void updateLecture(Lecture lecture);

   /**
    *  deletes given lecture
    *
    * @param lecture Lecture to delete
    */
   void deleteLecture(Lecture lecture);

}
