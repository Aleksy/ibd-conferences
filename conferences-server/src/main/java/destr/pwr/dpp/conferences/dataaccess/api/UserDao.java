package destr.pwr.dpp.conferences.dataaccess.api;

import destr.pwr.dpp.conferences.common.model.User;

import java.sql.SQLException;
import java.util.List;

/**
 * User data access object
 */
public interface UserDao {

    /**
     * Get all users from database
     *
     * @return list of users
     * @throws SQLException Database exception
     */
    List<User> getAllUsers() throws SQLException;

    /**
     * Get user by login from database
     *
     * @param login Users login
     * @return user object
     * @throws SQLException Database exception
     */
    User getUser(String login) throws SQLException;

    /**
     * update user data based on login
     * update firsttName, lastName, login, email, isStudent, isLecturer, price, isAlreadyPaid
     *
     * @param user User to update
     */
    void updateUser(User user);

    /**
     * delete given user
     * <p>
     *
     * @param user User to delete
     */
    void deleteUser(User user);

    /**
     * insert new user in database
     * <p>
     *
     * @param user user to register
     */
    void registerUser(User user);

    /**
     *  Retruns the list of users of certain payments status (alreadyPaid).
     *  Use example: Admin wants to check who did not pay or wants to get the list to verify payments.
     * @param isAlreadyPaid should get the list of users that already paid or not paid?
     * @return List of users of certain payments status
     * @throws SQLException Database exception
     */
    List<User> getAllUsersOfPaymentStatus(boolean isAlreadyPaid) throws SQLException;

}
