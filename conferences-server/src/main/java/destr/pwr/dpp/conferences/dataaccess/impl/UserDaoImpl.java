package destr.pwr.dpp.conferences.dataaccess.impl;

import destr.pwr.dpp.conferences.common.model.User;
import destr.pwr.dpp.conferences.dataaccess.api.UserDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link UserDao}
 */
@Repository
public class UserDaoImpl implements UserDao {

   @PersistenceContext
   private EntityManager entityManager;

   @Override
   @Transactional
   public List<User> getAllUsers() throws SQLException {
      List result = entityManager.createQuery("SELECT u FROM User u")
         .getResultList();
      List<User> map = new ArrayList<>();
      result.forEach(obj -> map.add((User) obj));
      return map;
   }

   @Override
   @Transactional
   public User getUser(String login) throws SQLException {
      Object user = entityManager.createQuery("SELECT u FROM User u " +
         "WHERE LOGIN = :login")
         .setParameter("login", login)
         .getResultList()
         .stream().findFirst().orElse(null);
      return (User) user;
   }

   @Override
   @Transactional
   public void updateUser(User user) {
      entityManager.createNativeQuery("UPDATE USER SET " +
         "FIRST_NAME=:firstName, LAST_NAME=:lastName, EMAIL=:email, LECTURER=:isLecturer, " +
         "PRICE=:price, ALREADY_PAID=:isAlreadyPaid, LECTURE_ID=:lectureId, PASSWORD=:password, " +
         "RODO_ACCEPTED=:rodoAccepted WHERE LOGIN=:login")
         .setParameter("firstName", user.getFirstName())
         .setParameter("lastName", user.getLastName())
         .setParameter("email", user.getEmail())
         .setParameter("isLecturer", user.isLecturer())
         .setParameter("price", user.getPrice())
         .setParameter("isAlreadyPaid", user.isAlreadyPaid())
         .setParameter("lectureId", user.getLectureId())
         .setParameter("password", user.getPassword())
         .setParameter("login", user.getLogin())
         .setParameter("rodoAccepted", user.isRodoAccepted())
         .executeUpdate();
   }

   @Override
   @Transactional
   public void deleteUser(User user) {
      entityManager.createNativeQuery("DELETE FROM USER WHERE LOGIN=:userLogin")
         .setParameter("userLogin", user.getLogin())
         .executeUpdate();
   }

   @Override
   @Transactional
   public void registerUser(User user) {
      entityManager.createNativeQuery("INSERT INTO USER " +
         "(ID, LOGIN, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL, ADMIN, LECTURER, " +
         "PRICE, ALREADY_PAID, LECTURE_REGISTERED, VERIFIED) VALUES " +
         "(0, :userLogin, :password, :firstName, :lastName, :email, :isAdmin, :isLecturer, :price, " +
         ":isAlreadyPaid, :isLectureRegistered, :isVerified)")
         .setParameter("userLogin", user.getLogin())
         .setParameter("password", user.getPassword())
         .setParameter("firstName", user.getFirstName())
         .setParameter("lastName", user.getLastName())
         .setParameter("email", user.getEmail())
         .setParameter("isAdmin", user.isAdmin())
         .setParameter("isLecturer", user.isLecturer())
         .setParameter("price", user.getPrice())
         .setParameter("isAlreadyPaid", user.isAlreadyPaid())
         .setParameter("isLectureRegistered", user.isLectureRegistered())
         .setParameter("isVerified", user.isVerified())
         .executeUpdate();
   }

   @Override
   @Transactional
   public List<User> getAllUsersOfPaymentStatus(boolean isAlreadyPaid) throws SQLException {
      List result = entityManager.createQuery("SELECT l FROM LECTURE l " +
         "WHERE ALREADY_PAID = :isAlreadyPaid")
         .setParameter("isAlreadyPaid", isAlreadyPaid)
         .getResultList();
      List<User> map = new ArrayList<>();
      result.forEach(obj -> map.add((User) obj));
      return map;
   }
}
