package destr.pwr.dpp.conferences.common.rest.impl;

import destr.pwr.dpp.conferences.common.model.*;
import destr.pwr.dpp.conferences.common.rest.api.ConferencesRestService;
import destr.pwr.dpp.conferences.dataaccess.api.ConferenceDao;
import destr.pwr.dpp.conferences.dataaccess.api.LectureDao;
import destr.pwr.dpp.conferences.dataaccess.api.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link ConferencesRestService}
 */
@Controller
@RestController
public class ConferencesRestServiceImpl implements ConferencesRestService {

   @Autowired
   private ConferenceDao conferenceDao;

   @Autowired
   private LectureDao lectureDao;

   @Autowired
   private UserDao userDao;

   @Override
   public int getPriceForUser(User user) {
      return user.isLecturer() ? 10 : 20;
   }

   @Override
   public UserWithLectureTo login(User user) throws SQLException {
      User tempUser = userDao.getUser(user.getLogin());
      if (tempUser == null) return null;

      if (tempUser.getPassword().equals(user.getPassword())) {
         UserWithLectureTo userWithLectureTo = new UserWithLectureTo();
         if(tempUser.isLecturer())
            userWithLectureTo.setLecture(lectureDao.getLecture(tempUser.getLectureId()));
         userWithLectureTo.setUser(tempUser);
         return userWithLectureTo;
      } else return null;
   }

   @Override
   public void registerUser(User user) throws SQLException {

      if (user.getLogin() == null || user.getLogin().isEmpty() ||
         user.getPassword() == null || user.getPassword().isEmpty()) return;
      user.setPassword(user.getPassword());
      userDao.registerUser(user);
   }

   @Override
   public void updateUser(UserWithLectureTo user) {
      userDao.updateUser(user.getUser());
      lectureDao.updateLecture(user.getLecture());
   }

   @Override
   public void deleteUser(UserWithLectureTo user) {
      userDao.deleteUser(user.getUser());
      lectureDao.deleteLecture(user.getLecture());
   }

   @Override
   public List<UserWithLectureTo> getAllUsers() throws SQLException {
      List<User> allUsers = userDao.getAllUsers();
      List<Lecture> allLectures = lectureDao.getAllLectures();
      return allUsers.stream()
         .map(user -> {
            UserWithLectureTo userWithLectureTo = new UserWithLectureTo();
            userWithLectureTo.setLecture(allLectures.stream()
               .filter(lecture -> Objects.equals(lecture.getId(), user.getLectureId()))
               .findFirst().orElse(null));
            userWithLectureTo.setUser(user);
            return userWithLectureTo;
         })
         .collect(Collectors.toList());
   }

   @Override
   public List<Lecture> getAllLectures() throws SQLException {
      return lectureDao.getAllLectures();
   }

   @Override
   public List<Lecture> getAllLecturesOfStatus(boolean isAccepted) throws SQLException {
      return lectureDao.getAllLecturesOfStatus(isAccepted);
   }

   @Override
   public void registerLecture(UserWithLectureTo userWithLectureTo) throws SQLException {
      // Store users lecture
      Lecture lecture = userWithLectureTo.getLecture();

      // Check if added lecture
      if (lecture != null) {

         // Check if not empty fields has been passed or nulls
         if (lecture.getTopic() == null || lecture.getTopic().isEmpty() ||
            lecture.getLectureAbstract() == null || lecture.getLectureAbstract().isEmpty()) return;

         userWithLectureTo.getUser().setLectureRegistered(true);

         // Add lecture to data base
         Lecture saved = lectureDao.newLecture(lecture);
         userWithLectureTo.getUser().setLectureId(saved.getId());
         userDao.updateUser(userWithLectureTo.getUser());
      }
   }

   @Override
   public void updateLecture(Lecture lecture) throws SQLException {
      if (lecture == null) return;
      lectureDao.updateLecture(lecture);
   }

   @Override
   public void updateLectures(List<Lecture> lecture) throws SQLException {
      lecture.forEach(l -> lectureDao.updateLecture(l));
   }

   @Override
   public void deleteLecture(Lecture lecture) throws SQLException {
      User user = userDao.getAllUsers().stream()
         .filter(u -> Objects.equals(u.getLectureId(), lecture.getId()))
         .findFirst().orElse(null);
      if(user != null) {
         user.setLecturer(false);
         user.setLectureId(null);
         user.setLectureRegistered(false);
         userDao.updateUser(user);
      }
      lectureDao.deleteLecture(lecture);
   }

   @Override
   public List<User> getAllUsersOfPaymentStatus(boolean isAlreadyPaid) throws SQLException {
      return userDao.getAllUsersOfPaymentStatus(isAlreadyPaid);
   }

   @Override
   public List<ConferenceWithLecturesTo> getAllConferences() throws SQLException {
      List<Conference> allConferences = conferenceDao.getAllConferences();
      if (allConferences.isEmpty()) {
         return null;
      }
      ConferenceWithLecturesTo conferenceWithLecturesTo = new ConferenceWithLecturesTo();
      conferenceWithLecturesTo.setConference(allConferences.get(0));
      List<Lecture> allLectures = lectureDao.getAllLectures();
      conferenceWithLecturesTo.setLectures(allLectures);
      return new ArrayList<>(Collections.singletonList(conferenceWithLecturesTo));
   }

   @Override
   public void clearConferenceData(Conference conference) throws SQLException {
      conferenceDao.deleteConference(conference);
      userDao.getAllUsers().forEach(user -> {
            if (!user.isAdmin()) {
               userDao.deleteUser(user);
            }
         }
      );
      lectureDao.getAllLectures().forEach(lecture -> {
         lectureDao.deleteLecture(lecture);
      });
   }

   @Override
   public void updateConference(Conference conference) throws SQLException {
      conferenceDao.updateConference(conference);
   }

   @Override
   public void registerConference(Conference conference) throws SQLException {
      conferenceDao.reigsterConference(conference);
   }
}
