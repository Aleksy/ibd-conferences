package destr.pwr.dpp.conferences.dataaccess.impl;

import destr.pwr.dpp.conferences.common.model.Lecture;
import destr.pwr.dpp.conferences.dataaccess.api.LectureDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * {@link LectureDao}
 */
@Repository
public class LectureDaoImpl implements LectureDao {

   @PersistenceContext
   private EntityManager entityManager;

   @Override
   @Transactional
   public Lecture getLecture(Integer lectureId) throws SQLException {
      if (lectureId != null) {
         Object lecture = entityManager.createQuery("SELECT lec FROM Lecture lec " +
            "WHERE ID = :lectureId")
            .setParameter("lectureId", lectureId)
            .getSingleResult();
         return (Lecture) lecture;
      }
      return null;
   }

   @Override
   @Transactional
   public List<Lecture> getAllLectures() throws SQLException {
      List result = entityManager.createQuery("SELECT lec FROM Lecture lec ORDER BY lec.timeAndDate")
         .getResultList();
      List<Lecture> map = new ArrayList<>();
      result.forEach(obj -> map.add((Lecture) obj));
      return map;
   }

   @Override
   @Transactional
   public List<Lecture> getAllLecturesOfStatus(boolean isAccepted) throws SQLException {

      List result = entityManager.createQuery("SELECT lec FROM Lecture lec " +
         "WHERE ACCEPTED = :isAccepted")
         .setParameter("isAccepted", isAccepted)
         .getResultList();
      List<Lecture> map = new ArrayList<>();
      result.forEach(obj -> map.add((Lecture) obj));
      return map;
   }

   @Override
   @Transactional
   public Lecture newLecture(Lecture lecture) throws SQLException {
      entityManager.createNativeQuery("INSERT INTO LECTURE " +
         "(ID, TOPIC, ABSTRACT, " +
         "LOCALIZATION, TIME_AND_DATE, ACCEPTED) VALUES " +
         "(0, :lectureTopic, :lectureAbstract, " +
         ":localization, :timeAndData, :isAccepted)")
         .setParameter("lectureTopic", lecture.getTopic())
         .setParameter("lectureAbstract", lecture.getLectureAbstract())
         .setParameter("localization", lecture.getLocalization())
         .setParameter("timeAndData", lecture.getTimeAndDate())
         .setParameter("isAccepted", lecture.isAccepted())
         .executeUpdate();
      return this.getAllLectures().stream().filter(l -> Objects.equals(l, lecture)).findFirst().get();
   }

   @Override
   @Transactional
   public void updateLecture(Lecture lecture) {
      if (lecture != null)
         entityManager.createNativeQuery("UPDATE LECTURE SET " +
            "TOPIC=:topic, LOCALIZATION=:localization, ACCEPTED=:isAccepted, " +
            "TIME_AND_DATE=:timeAndDate, ABSTRACT=:abstract WHERE ID=:lectureId")
            .setParameter("topic", lecture.getTopic())
            .setParameter("localization", lecture.getLocalization())
            .setParameter("isAccepted", lecture.isAccepted())
            .setParameter("timeAndDate", lecture.getTimeAndDate())
            .setParameter("abstract", lecture.getLectureAbstract())
            .setParameter("lectureId", lecture.getId())
            .executeUpdate();
   }

   @Override
   @Transactional
   public void deleteLecture(Lecture lecture) {
      if (lecture != null)
         entityManager.createNativeQuery("DELETE FROM LECTURE WHERE ID=:id")
            .setParameter("id", lecture.getId())
            .executeUpdate();
   }
}
