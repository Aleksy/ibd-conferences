package destr.pwr.dpp.conferences.dataaccess.impl;

import destr.pwr.dpp.conferences.common.model.Conference;
import destr.pwr.dpp.conferences.common.model.Lecture;
import destr.pwr.dpp.conferences.common.model.User;
import destr.pwr.dpp.conferences.dataaccess.api.ConferenceDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link ConferenceDao}
 */
@Repository
public class ConferenceDaoImpl implements ConferenceDao {

   @PersistenceContext
   private EntityManager entityManager;

   @Override
   @Transactional
   public List<Conference> getAllConferences() {
      List result = entityManager.createQuery("SELECT c FROM Conference c")
              .getResultList();
      List<Conference> map = new ArrayList<>();
      result.forEach(obj -> map.add((Conference) obj));
      return map;
   }

   @Override
   @Transactional
   public void reigsterConference(Conference conference) {
      entityManager.createNativeQuery("INSERT INTO CONFERENCE " +
              "(ID, TITLE, HOST, " +
              "DATE_AND_TIME, LOCALIZATION) VALUES " +
              "(0, :title, :host, :dateAndTime, :localization)")
              .setParameter("title", conference.getTitle())
              .setParameter("host", conference.getHost())
              .setParameter("dateAndTime", conference.getDateAndTime())
              .setParameter("localization", conference.getLocalization())
              .executeUpdate();
   }

   @Override
   @Transactional
   public void updateConference(Conference conference) {
      entityManager.createNativeQuery("UPDATE CONFERENCE SET " +
              "TITLE=:title, HOST=:host, DATE_AND_TIME=:dateAndTime, " +
              "LOCALIZATION=:localization")
              .setParameter("title", conference.getTitle())
              .setParameter("host", conference.getHost())
              .setParameter("dateAndTime", conference.getDateAndTime())
              .setParameter("localization", conference.getLocalization())
              .executeUpdate();
   }

   @Override
   @Transactional
   public void deleteConference(Conference conference) {
      entityManager.createNativeQuery("DELETE FROM CONFERENCE").executeUpdate();
   }
}
